import 'raf/polyfill'
import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

global.React = React

Enzyme.configure({ adapter: new Adapter() })

