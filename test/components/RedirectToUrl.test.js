import RedirectToUrl from '../../src/components/RedirectToUrl'
import { shallow, mount } from 'enzyme'

import actions from '../../src/actions'

describe('RedurectToUrl', () => {
  beforeEach(() => {
    jest.mock('../../src/actions')
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  test('should not render anything when no url is given', () => {
    const component = shallow(<RedirectToUrl />)
    expect(component.text()).toEqual('')
  })

  test('should display url being directed to', async (done) => {
    let promise = Promise.resolve('http://test.com')
    const props = {
      match: {
        params: {
          urlCode: 1234
        }
      }
    }
    actions.getUrl = jest.fn().mockImplementation(() => promise)

    const component = mount(<RedirectToUrl {...props} />)
    await promise

    expect(component.text()).toEqual('Redirecting to http://test.com')
    done()
  })
})