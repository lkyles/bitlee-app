import ShortenUrl from '../../src/components/ShortenUrl'
import { mount } from 'enzyme'

describe('ShortenUrl', () => {
  test('should display message when form subbmission throws error', () => {
    const component = mount(<ShortenUrl />)

    component.setState({ error: true })
    component.update()

    expect(component.text()).toContain('Sorry, there has been a problem trying to shorten this URL. Please try again.')
  })

  test('should new shortended URL', () => {
    const component = mount(<ShortenUrl />)

    component.setState({ results: {shortUrl: 'test.com'} })
    component.update()

    expect(component.text()).toContain('test.com')
  })
})