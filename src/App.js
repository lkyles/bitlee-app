import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import RedirectToUrl from './components/RedirectToUrl'
import ShortenUrl from './components/ShortenUrl'
import Header from './components/Header'
import styled from 'react-emotion'

const Container = styled.div({
  textAlign: 'center'
})

class App extends Component {
  render() {
    return (
      <Container>
        <Header />
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={ShortenUrl}/>
            <Route path='/:urlCode' exact component={RedirectToUrl}/>
          </Switch>
        </BrowserRouter>
      </Container>
    );
  }
}

export default App;
