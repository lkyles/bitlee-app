import axios from 'axios'
import { API_HOST } from './constants'

const getUrl = (url) => axios.get(`${API_HOST}/url/${url}`)
  .then(response => Promise.resolve(response.data.url) )

const postUrl = (urlDetails) => axios.post(`${API_HOST}/url`, urlDetails)
  .then(response => Promise.resolve(response.data))
  .catch(() => Promise.reject())

export default {
  getUrl,
  postUrl
}