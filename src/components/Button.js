import React from 'react'
import styled from 'react-emotion'

const ButtonComponent = styled.button({
  width: '100%',
  flex: '1 0 100%',
  fontWeight: 500,
  minHeight: 40,
  borderStyle: 'none',
  borderRadius: 24,
  borderColor: null,
  boxShadow: ('0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 2px 0 rgba(0, 0, 0, 0.4)'),
  color: '#fbfbfb',
  textAlign: 'center',
  backgroundColor: '#704e8e',
  ':hover': {
    boxShadow: ('0 3px 4px 0 rgba(0, 0, 0, 0.5)')
  },
  ':active': {
    boxShadow: '0 1px 1px 0 rgba(0, 0, 0, 0.3), inset 0 3px 2px 0 rgba(0, 0, 0, 0.23)',
    outline: 'none',
    backgroundColor: '#704e8e'
  },
  ':focus': {
    outline: 'none'
  },
  ':disabled': {
    backgroundColor: '#a298ab',
    borderStyle: 'none',
    borderWidth: 0,
    borderColor: null,
    color: '#fbfbfb',
    boxShadow: 'none',
    opacity: 1
  }
})


class Button extends React.Component {
  render () {
    const { dataTest, type = 'button', children, clickHandler, disabled = false } = this.props

    const clickHandlerWrapper = event => {
      if (clickHandler) {
        event.preventDefault()
        clickHandler()
      }
    }

    return (
      <ButtonComponent data-test={dataTest} type={type} onClick={clickHandlerWrapper} disabled={disabled}>
        {children}
      </ButtonComponent>
    )
  }
}

export default Button
