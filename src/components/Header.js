import React from 'react'
import styled from 'react-emotion'

const Container = styled.header({
  backgroundColor: '#704e8e',
  padding: '20px',
  color: 'white',
  textAlign: 'center',
  marginBottom: '20px'
})

const Title = styled.h1()

const Header = () => (
  <Container>
    <Title>Welcome to bitlee</Title>
  </Container>
)

export default Header
