import React from 'react'
import actions from '../actions'

class RedirectToUrl extends React.Component {
  constructor(props) {
    super(props)
    this.state = {url: ''}
  }

  componentWillMount() {
    if (this.props.match && this.props.match.params.urlCode) {
      actions.getUrl(this.props.match.params.urlCode)
      .then(url => this.setState({url}))
    }
  }

  render() {
    const redirect = (url) => {
      window.location = url
      return url
    }
    return this.state.url ? (
      <h4>Redirecting to {redirect(this.state.url)}</h4>
    ) : null
  }
}

export default RedirectToUrl
