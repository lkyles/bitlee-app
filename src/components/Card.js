import React from 'react'
import styled from 'react-emotion'

const Container = styled.div({
  backgroundColor: '#ffffff',
  maxWidth: '500px',
  margin: 'auto'
})

const Title = styled.div({
  backgroundColor: '#e4e4e4',
  padding: `16px`,
  fontWeight: 500,
  lineHeight: 1.3,
  borderBottom: `1px solid #ececec`
})

const Body = styled.div({
  backgroundColor: '#ffffff',
  padding: '16px'
})

const Card = ({title, children}) => (
  <Container>
    {title ? <Title>{title}</Title> : null}
    <Body>
      {children}
    </Body>
  </Container>
)

export default Card