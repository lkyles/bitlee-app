import React from 'react'
import styled from 'react-emotion'
import { URL_DOMAIN } from '../constants'
import actions from '../actions'
import Button from './Button'

import Card from './Card'

const Row = styled.div({
  padding: '8px 0',
  maxWidth: '400px',
  margin: 'auto'
})

const Error = styled.h4({
  color: '#a74444'
})

const Input = styled.input({
  width: '100%',
  maxWidth: '378px'
})

const Link = styled.a({
  color: '#a063b7'
})

class ShortenUrl extends React.Component {
  constructor(props) {
    super(props)
    this.state = {url: '', domain: URL_DOMAIN, results: null, error: null}
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value})
  }

  handleSubmit(event) {
    console.log('fsdfsdfsd');
    event.preventDefault()
    actions.postUrl({originalUrl: this.state.url, domain: this.state.domain})
    .then(results => this.setState({results}))
    .catch(() => this.setState({error: true}))
  }

  render() {
    return (
      <div>
        {this.state.error ? <Error>Sorry, there has been a problem trying to shorten this URL. Please try again.</Error> : null}
        <form onSubmit={this.handleSubmit}>
          <Row>
            <Input type="url" name="url" placeholder="Enter URL" value={this.state.url} onChange={this.handleChange} />
          </Row>
          <Row>
            <Button type='submit' disabled={this.state.url === ''}>Submit</Button>
          </Row>
        </form>
        {this.state.results ? (
          <Row>
            <Card title='Your new URL'>
              <Link href={this.state.results.shortUrl} target='_blank'>
                {this.state.results.shortUrl}
              </Link>
            </Card>
          </Row>
        ) : null}
      </div>)
  }
}

export default ShortenUrl
