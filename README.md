#### bitlee App

This is the frontend component part of a coding challenge application that provides URL shortening.

This application was built with create-react-app

#### Running the application

visit the deployed version at https://bitlee.org

You can run locally with
```
npm install
npm start
```

#### Testing the application

To test the app run 
```
npm test
```

#### Deployment

The frontend app is deployed on AWS S3 and distrubuted via cloudfront. Its deployed via the bitbucket pipeline
